package com.tproject.demoreport;

import java.util.Random;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import com.tproject.report.Report;
import com.tproject.report.tools.StringReport;

public class Demo extends Shell {

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			Demo shell = new Demo(display);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the shell.
	 * @param display
	 */
	public Demo(Display display) {
		super(display, SWT.SHELL_TRIM);
		Button btnShowReport = new Button(this, SWT.MAX);
		btnShowReport.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String isi_header ="<table class=\"detail\">";
				isi_header += "<CAPTION>";
				isi_header += "DEMO REPORT";
				isi_header += "</CAPTION>";
				isi_header += "<thead>";
				isi_header += "<tr class=\"color1\">";
					isi_header += "<th>No</th>";
					isi_header += "<th>Nama</th>";
					isi_header += "<th>Alamat</th>";
					isi_header += "<th>Angka</th>";
				isi_header += "</tr>";
				isi_header += "</thead>";
				isi_header += "<tbody>";
				String isi = isi_header;
				String warna = "";
				for (int a = 1 ; a<100 ; a++){
					if(a%2==0){
						warna = "class=\"color2\"";
					}else{
						warna = "";
					}
					isi += "<tr " + warna + " >";
						isi += "<td class='clstdcenter'>";
						isi += a;
						isi += "</td>";
						isi += "<td>";
						isi += "Nama " + a;
						isi += "</td>";
						isi += "<td>";
						isi += "Alamat " + a;
						isi += "</td>";
						isi += "<td  class='clstdnumber'>";
						isi +=  a * 100;
						isi += "</td>";
					isi += "</tr>";
				}
				isi += "</tbody>";
				isi += "<tfoot>";
					isi += "<tr>";
						isi += "<td colspan=\"4\" class=\"clstdcenter\" >Ini Footer</td>";
					isi += "</tr>";
				isi += "</tfoot>";
				isi += "</table>";
				StringReport strRpt= new StringReport(isi, "");
				//strRpt.setFooter("Sugik Puja Kusuma");
				new Report( getShell(), SWT.NONE ).open( strRpt.getHtml() );
			}
		});
		btnShowReport.setBounds(10, 10, 75, 25);
		btnShowReport.setText("Show Report");
		createContents();
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("Demo");
		setSize(450, 300);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
