package com.tproject.report.tools;

public class StringReport {
	private String content;
	private String css = "", footer = "";
	
	public StringReport(String content, String css) {
		this.content = content;
		this.css = css;
		setDefaultCss();
	}
	
	private void setDefaultCss(){
		css += " @media screen {" +
				"    div.divFooter , .page-break{" +
				"            display: none;" +
				"       }" +
				" } " +
				" @media print {" +
				"	div.divFooter {" +
				"   	position: fixed;" +
				"       bottom: 0;" +
				"   }" +
				"" +
				"	.page-break{" +
				"    	page-break-before:always;" +
				" 	} " +
				"" +
				"  	table { page-break-after: auto } " +
				"  	tr    { page-break-inside:avoid; page-break-after:auto } " +
				"  	td    { page-break-inside:avoid; page-break-after:auto } " +
				"  	thead { display:table-header-group } " +
				"  	tfoot { display:table-footer-group } " +
				"" +
				" } " ;
		/*
		 always
		<style tyle="text/css">
		<!--
		@page { size : portrait }
		@page rotated { size : landscape }
		table { page : rotated }
		-->
		</style>
		 * */
		//solid
		css += " @page{" +
				"	margin: 10px 10px 10px 10px;" +
				"	size : landscape;" +
				"} " +
				"table.header{" +
				"    font-size: 14pt;" +
				"    font-family:Tahoma, sans-serif;" +
				"    width: 100%;" +
				"} " +
				"table.detail{" +
				"   font-size: 12pt;" +
				"   border-collapse:collapse;" +
				"   font-family:Tahoma, sans-serif;" +
				"   width: 100%;" +
				"}" +
				"" +
				"table.detail td,table.detail th,table.detail tr{" +
				"    border: 1px solid black;" +
				"} " +
				"" +
				".clstdnumber{" +
				"    text-align: right;" +
				"}" +
				"" +
				".flKanan{" +
				"    float: right;" +
				"}" +
				"" +
				".color1{" +
				"    background-color:#D6D6D6" +
				"}" +
				"" +
				".color2{" +
				"    background-color:#F4F4F4" +
				"}" +
				"" +
				".clstdcenter{" +
				"    text-align: center;" +
				"} ";
		css += " table.detail tfoot tr td{ " +
				"	border-right-style: none; " +
				"	border-left-style: none;" +
				"	border-bottom-style: none;" +
				"}";
	}

	public String getCss() {
		return css;
	}

	public void setCss(String css) {
		this.css = css;
	}
	
	public String getHtml(){
		String html ; 
		html="<html>" + "\n";
		html +="<head>" + "\n";
		html +="<style>" + "\n";
		html +=css + "\n";
		html +="</style>" + "\n";
		html +="</head>" + "\n";
		html +="<body>" + "\n";
		html += content + "\n";
		if (!footer.equals("")){
			html += "<div class=\"divFooter clstdcenter\" >" + "\n";
			html += footer + "\n";
			html += "<div>" + "\n";
		}
		html +="</body>" + "\n";
		html +="</html>";
		return html;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

}
