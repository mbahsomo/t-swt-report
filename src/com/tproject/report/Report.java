package com.tproject.report;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.browser.Browser;
import swing2swt.layout.BorderLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.wb.swt.SWTResourceManager;

public class Report extends Dialog {

	protected Object result;
	protected Shell shlPreview;
	private Browser browser;
	private ToolItem tltmClose;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public Report(Shell parent, int style) {
		super(parent, style);
		setText("Print Preview by do-event.com");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open(String isiRpt) {
		createContents();
		browser.setText(isiRpt);
		shlPreview.open();
		shlPreview.layout();
		Display display = getParent().getDisplay();
		while (!shlPreview.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlPreview = new Shell(getParent(), SWT.SHELL_TRIM | SWT.BORDER);
		shlPreview.setImage(SWTResourceManager.getImage(Report.class, "/images/print.png"));
		//shlPreview.setSize(468, 448);
		shlPreview.setText("Print Preview by mbahsomo@do-event.com");
		shlPreview.setLayout(new BorderLayout(0, 0));
		
		browser = new Browser(shlPreview, SWT.NONE);
		
		ToolBar toolBar = new ToolBar(shlPreview, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(BorderLayout.NORTH);
		
		ToolItem tltmCetak = new ToolItem(toolBar, SWT.NONE);
		tltmCetak.setImage(SWTResourceManager.getImage(Report.class, "/images/print.png"));
		tltmCetak.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				browser.execute("javascript:window.print();");
			}
		});
		tltmCetak.setText("Cetak");
		
		tltmClose = new ToolItem(toolBar, SWT.NONE);
		tltmClose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shlPreview.dispose();
			}
		});
		tltmClose.setImage(SWTResourceManager.getImage(Report.class, "/images/exit.png"));
		tltmClose.setText("Keluar");
		
	}
}
